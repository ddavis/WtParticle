## CMakeLists.txt for WtParticle package to be built within ATLAS
## Analysis Relase 21.2.X series.
## Author: Doug Davis <ddavis@cern.ch>

# Declare the name of this package:
atlas_subdir( WtParticle None )


# This package depends on other packages:
atlas_depends_on_subdirs( PUBLIC
  TopLoop
  )

# This package uses ROOT:
find_package( ROOT REQUIRED COMPONENTS Physics Core Tree Hist RIO MathCore TreePlayer )

# Build a library that other components can link against:
atlas_add_library( WtParticle
  WtParticle/*.h Root/*.cxx Root/*.h
  PUBLIC_HEADERS WtParticle
  SHARED
  LINK_LIBRARIES TopLoop
  PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES} ${YAML_CPP_LIBRARIES}
  PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS} ${YAML_CPP_INCLUDE_DIRS})

target_include_directories( WtParticle PRIVATE ${CMAKE_BINARY_DIR}/generated )

add_dependencies( WtParticle yaml-cpp )

atlas_add_dictionary( WtParticleDict
  WtParticle/WtParticleDict.h WtParticle/selection.xml
  LINK_LIBRARIES WtParticle )

# Install data files from the package:
atlas_install_data( data/* )

# Install python modules
atlas_install_python_modules( python/* )

# Install scripts
atlas_install_scripts( scripts/* )

atlas_add_executable( runWtParticle
  util/runWtParticle.cxx
  LINK_LIBRARIES ${ROOT_LIBRARIES} WtParticle TopLoop )

// Analysis
#include <WtParticle/WtParticleAlg.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <TopLoop/EDM/Helpers.h>

wt2::WtParticleAlg::WtParticleAlg() {}

wt2::WtParticleAlg::~WtParticleAlg() {}

TL::StatusCode wt2::WtParticleAlg::init() {
  TL_CHECK(TL::Algorithm::init());

  m_dsid         = get_dsid();

  m_totalNomWeights = weightTool().generatorSumWeights();
  m_lumiWeight      = weightTool().luminosityWeight({TL::kCampaign::MC16d},1000.0);

  logger()->info("DSID: {}, cross section: {} pb", m_dsid, weightTool().sampleCrossSection());
  logger()->info("Campaign: {}",TL::SampleMetaSvc::get().
                 getCampaignStr(fileManager()->rucioDir(),false));
  TL::SampleMetaSvc::get().printInfo(m_dsid);

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::setupOutput() {
  TL_CHECK(TL::Algorithm::setupOutput());

  std::unique_ptr<TFile> outfileptr(TFile::Open(m_outFileName.c_str(),"RECREATE"));
  m_outFile = std::move(outfileptr);

  m_outTreeName = "WtParticle_"+fileManager()->treeName();
  m_WtParticleFlat = new TTree(m_outTreeName.c_str(),m_outTreeName.c_str());

  TL_CHECK(defineOutputVariables(m_WtParticleFlat));
  TL_CHECK(defineOutputWeights(m_WtParticleFlat));

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::execute() {
  TL_CHECK(TL::Algorithm::execute());

  // if no electrons skip event
  if ( PL_el_pt().empty() ) return TL::StatusCode::SUCCESS;
  // if no muons skip event
  if ( PL_mu_pt().empty() ) return TL::StatusCode::SUCCESS;
  // if no jets skip event
  if ( PL_jet_pt().empty() ) return TL::StatusCode::SUCCESS;

  m_weight_nominal = m_lumiWeight * PL_weight_mc() * PL_weight_pileup();

  m_pT_lep1 = std::max(PL_el_pt().at(0), PL_mu_pt().at(0));
  m_pT_lep2 = std::min(PL_el_pt().at(0), PL_mu_pt().at(0));
  m_pT_jet1 = PL_jet_pt().at(0);

  m_nbjets = 0;
  m_njets  = 0;
  for ( std::size_t i = 0; i < PL_jet_pt().size(); ++i ) {
    if ( PL_jet_pt().at(i) > 25000 ) {
      m_njets++;
    }
    if ( PL_jet_nGhosts_bHadron().at(i) > 0 ) {
      m_nbjets++;
    }
  }

  m_WtParticleFlat->Fill();

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::finish() {
  TL_CHECK(TL::Algorithm::finish());
  m_outFile->Write();
  m_outFile->Close();
  return TL::StatusCode::SUCCESS;
}

#include <WtParticle/WtParticleAlg.h>

#include <regex>

TL::StatusCode wt2::WtParticleAlg::init_additional_vars() {
  // where we would implement any SgTop ntuple variable access that is
  // not provided by default in TopLoop. TopLoop is currently
  // configured for SgTop ntuples though!
  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::defineOutputVariables(TTree* tree) {
  if ( tree == nullptr ) {
    return TL::StatusCode::FAILURE;
  }

  auto make_branch = [this,tree](const char* name, auto* v) -> void {
                       /*
                         for ( auto const& exclude : this->config()->excludes() ) {
                         std::regex r{exclude};
                         if ( std::regex_search(name,r) ) return;
                         }
                       */
                       tree->Branch(name,v);
                     };

  make_branch("njets", &m_njets);
  make_branch("nbjets",&m_nbjets);

  make_branch("pT_lep1",&m_pT_lep1);
  make_branch("pT_lep2",&m_pT_lep2);
  make_branch("pT_jet1",&m_pT_jet1);

  return TL::StatusCode::SUCCESS;
}

TL::StatusCode wt2::WtParticleAlg::defineOutputWeights(TTree* tree) {
  if ( tree == nullptr ) {
    return TL::StatusCode::FAILURE;
  }

  auto make_branch = [this,tree](const char* name, auto* v) -> void {
                       /*
                         for ( auto const& exclude : this->config()->weight_excludes() ) {
                         std::regex r{exclude};
                         if ( std::regex_search(name,r) ) return;
                         }
                       */
                       tree->Branch(name,v);
                     };

  make_branch("weight_nominal",&m_weight_nominal);

  return TL::StatusCode::SUCCESS;
}

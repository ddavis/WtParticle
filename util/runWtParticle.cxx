// ROOT
// #include <TError.h>

// TL and AL
#include <WtParticle/WtParticleAlg.h>
#include <TopLoop/Core/Job.h>
#include <TopLoop/Core/FileManager.h>
#include <TopLoop/Core/SampleMetaSvc.h>
#include <WtParticle/Externals/CLI11.hpp>

// C++
#include <iostream>
#include <memory>

std::unique_ptr<TL::FileManager> build_fm_from_dir(const std::string& dirname) {
  auto fm = std::make_unique<TL::FileManager>();
  fm->enableParticleLevel();
  fm->feedDir(dirname);
  return fm;
}

TL::StatusCode runAlgo(std::unique_ptr<TL::FileManager> fm,
                       const std::string& out_file_name,
                       const bool disableProgress) {
  if ( fm->fileNames().empty() ) {
    spdlog::get("runWtParticle")->error("Empty FileManager!");
    return TL::StatusCode::FAILURE;
  }
  TL::Job job;
  if ( disableProgress ) {
    job.disableProgressBar();
  }
  job.setLoopType(TL::LoopType::ParticleAll);
  auto loopAlg = std::make_unique<wt2::WtParticleAlg>();
  loopAlg->setOutFileName(out_file_name);
  TL_CHECK(job.setFileManager(std::move(fm)));
  TL_CHECK(job.setAlgorithm(std::move(loopAlg)));
  return job.run();
}

int main(int argc, char *argv[]) {
  spdlog::set_pattern("[%n] [%l] %v");
  auto runnerlog = spdlog::stdout_color_mt("runWtParticle");
  TL::StatusCode::enableFailure();

  CLI::App app("WtParticle");
  std::string inDir, outFile;
  bool        debug, disableProgress;
  app.add_option("-d,--in-dir",inDir,"Path to directory containing files to process")
    ->required()->check(CLI::ExistingDirectory);
  app.add_option("-o,--out-file",outFile,"Name of output file")->required();
  app.add_flag("--debug",debug,"Print debugging statements");
  app.add_flag("--disable-progress",disableProgress,"disable progress bar");

  CLI11_PARSE(app,argc,argv);

  if ( debug ) { spdlog::set_level(spdlog::level::debug); }

  // always do the job from the command line
  auto fm = build_fm_from_dir(inDir);
  auto sample_dsid = fm->dsid();
  TL_CHECK(runAlgo(std::move(fm),outFile,disableProgress));

  return 0;
}

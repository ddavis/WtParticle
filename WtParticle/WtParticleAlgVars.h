/** @file  WtParticleAlgVars.h
 *  @brief WtParticleAlgVars class header
 *  @class WtParticleAlgVars
 *  @brief Variables container for hte WtParticleAlg algorithm
 *
 *  A clean place to hold all variables
 *
 *  @author Douglas Davis, <ddavis@cern.ch>
 */

#ifndef WtParticle_WtParticleAlgVars_h
#define WtParticle_WtParticleAlgVars_h


#include <TopLoop/Core/Variables.h>

namespace wt2 {
  class WtParticleAlgVars {

  protected:

    /// meta variables
    int   m_dsid{0};
    float m_totalNomWeights{0};

    float m_lumiWeight;

    uint32_t m_njets;
    uint32_t m_nbjets;

    float m_pT_lep1;
    float m_pT_lep2;
    float m_pT_jet1;


  protected:

    // weight variables
    float m_weight_nominal;

  public:
    WtParticleAlgVars() = default;
    virtual ~WtParticleAlgVars() = default;

    WtParticleAlgVars(const WtParticleAlgVars&) = delete;
    WtParticleAlgVars(WtParticleAlgVars&&) = delete;
    WtParticleAlgVars& operator=(const WtParticleAlgVars&) = delete;
    WtParticleAlgVars& operator=(WtParticleAlgVars&&) = delete;

  };
}

#endif

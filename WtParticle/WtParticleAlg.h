/** @file  WtParticleAlg.h
 *  @brief WtParticleAlg class header
 *  @class WtParticleAlg
 *  @brief TopLoop based algorith for Wt analysis (PL)
 *
 *  A class to process SingleTop particle level ntuples
 *
 *  @author Douglas Davis, <ddavis@cern.ch>
 */

#ifndef WtParticle_WtParticleAlg_h
#define WtParticle_WtParticleAlg_h

// TopLoop
#include <TopLoop/Core/Algorithm.h>
#include <TopLoop/EDM/FinalState.h>
#include <TopLoop/Core/SampleMetaSvc.h>

// WtParticle
#include <WtParticle/WtParticleAlgVars.h>

// ROOT
#include <TFile.h>

namespace wt2 {

  class WtParticleAlg : public TL::Algorithm, public wt2::WtParticleAlgVars {

  private:
    std::string m_outFileName{"unnamed_file.root"};
    std::string m_outTreeName{"unnamed_tree"};

    std::unique_ptr<TFile> m_outFile {nullptr};
    TTree* m_WtParticleFlat {nullptr};

  public:
    WtParticleAlg();
    virtual ~WtParticleAlg();
    WtParticleAlg(WtParticleAlg&&) = default;
    WtParticleAlg& operator=(const WtParticleAlg&) = default;
    WtParticleAlg& operator=(WtParticleAlg&&) = default;

    /// set the output ROOT file name
    void setOutFileName(const std::string& name);

    TL::StatusCode init()        final;
    TL::StatusCode setupOutput() final;
    TL::StatusCode execute()     final;
    TL::StatusCode finish()      final;

  private:
    // additional variable setup
    TL::StatusCode init_additional_vars();

    /// sets up output weight branches
    TL::StatusCode defineOutputWeights(TTree* tree);
    /// sets up the output branches
    TL::StatusCode defineOutputVariables(TTree* tree);

    /// reset any variables that are event dependent
    void resetEventProperties();

  };

  inline void WtParticleAlg::setOutFileName(const std::string& name) {
    m_outFileName = name;
  }

}

#endif
